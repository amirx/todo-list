package com.amirhb.letsDoList.ui.home

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.amirhb.letsDoList.Task
import kotlinx.coroutines.ExperimentalCoroutinesApi

private const val TAG = "HomeViewModel"

@ExperimentalCoroutinesApi
class HomeViewModel
@ViewModelInject
constructor(
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val tasks = arrayListOf<Task>()
    private val tasksLiveData = MutableLiveData<ArrayList<Task>> ()

    init {
        initTasks()
    }

    fun getTasks(): LiveData<ArrayList<Task>> {
        Log.d(TAG, "adding tasks $this")
        return tasksLiveData
    }

    fun addTask() {
        addedItem()
    }

    private fun initTasks(){
        Log.d(TAG, "adding tasks")
        tasks.add(Task("t1tr", true))
        tasks.add(Task("task2", false))
        tasksLiveData.value = tasks;
    }

    private fun addedItem(){
        Log.d(TAG, "adding item $this")
        tasks.add(Task("some_task", true) )
        tasksLiveData.value = tasks
        tasksLiveData.postValue(tasks)
    }


}