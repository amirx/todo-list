package com.amirhb.letsDoList.ui.edit

import android.os.Bundle
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.amirhb.letsDoList.R
import androidx.activity.viewModels


class EditTaskActivity : AppCompatActivity() {

    val ViewModel : EditTaskModelView by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_task)

        val modelView : EditTaskModelView by viewModels()

        val back_button: ImageButton = findViewById(R.id.edit_task_back_button)
        val done_button: ImageButton = findViewById(R.id.edit_task_done_button)

        back_button.setOnClickListener {finish();}
        done_button.setOnClickListener {modelView.save_values(); finish();}

    }
}