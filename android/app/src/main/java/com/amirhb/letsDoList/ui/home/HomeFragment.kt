package com.amirhb.letsDoList.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amirhb.letsDoList.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.ExperimentalCoroutinesApi

private const val TAG = "HomeFragment"

class HomeFragment : Fragment() {

    private val homeViewModel: HomeViewModel by activityViewModels()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView(view.findViewById(R.id.recycler_view))
        setupObserver()
        setupAddButton(view.findViewById(R.id.add_new_task_button))
    }

    private fun setupAddButton(addButton: FloatingActionButton) {
        addButton.setOnClickListener {
            homeViewModel.addTask()
        }
    }

    @ExperimentalCoroutinesApi
    private fun setupObserver() {
        Log.d(TAG, "setting up observers");
        homeViewModel.getTasks().observe(viewLifecycleOwner, {
            Log.d(TAG, "ooh observing changes");
            adapter.notifyDataSetChanged()
        })
    }

    fun initRecyclerView(recyclerView: RecyclerView) {
        Log.d(TAG, "INIT recycler view");
        adapter = RecyclerViewAdapter(
            recyclerView.context,
            homeViewModel.getTasks().value
        );
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context);
    }




}