import sqlalchemy

from letsdolist.views import task_list

def main():
    # Clear screen
    task_list()

try:
    main()
except sqlalchemy.exc.OperationalError:
    delete_db = input("oops there was a problem with the fabric of time and space, should I deleting the db before exiting? YES/whatever :")
    import os
    if delete_db == "yes":
        print(os.getcwd())
        os.remove("tasks_list.db")
        print("File Removed!")

