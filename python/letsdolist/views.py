from dateutil.parser import parse

from letsdolist.controller import Controller
from letsdolist.db_entities import SessionFactory
from datetime import datetime


controller = Controller(SessionFactory("sqlite:///tasks_list.db"))


def task_list():
    while True:
        tasks = controller.get_tasks()
        for i, task in enumerate(tasks):
            print(f"{task.id} : {task.due_date} : {task.title}")

        choice = input("(q)uit, (n)ew, (m) for multi-add, # + e to edit, # + d to mark done")

        if choice.lower() == "q":
            return
        elif choice.lower() == "n":
            new_task()
        elif choice.lower() == "m":
            multiadd()
        elif item_number_and_job := get_int_value(choice):
            item_number, job = item_number_and_job
            if job == "e":
                edit_task(item_number)
            elif job == "d":
                mark_done_task(item_number)
        else:
            print("didn't understand that!")

def new_task():
    title = input("title: ")
    description = input("description: ")
    due_date = input_date_and_parse("due_date: ")
    controller.add_task(title, description, due_date)


def multiadd():
    count = 0
    while title := input("title or empty line to exit: "):
        controller.add_task(title, "", None)
        count += 1
    print(f"added {count} tasks")

def mark_done_task(item_number):
    controller.mark_done(item_number, datetime.now())


import re
def get_int_value(choice):
    matches = re.match(r"\s*(\d+)\s*([de])\s*", choice)
    if matches:
        return int(matches.group(1)), matches.group(2)
    else:
        return None



def input_date_and_parse(message, default=None):
    while True:
        default_part = (f"[{default}]" if default is None else "")
        the_date = input(message + default_part)
        if the_date == "":
            return default
        else:
            try:
                return parse(the_date)
            except Exception:
                pass


def edit_task(item_number):
    task = controller.get_task(item_number)
    task.title = input_or_empty("title: ", task.title)
    task.description = input_or_empty("description: ", task.description)
    task.date = input_date_and_parse("due_date: ", task.due_date)
    controller.update_task(task)


def input_or_empty(message, default):
    input_val = input(message + f"[{default}]")
    if input_val.strip() == "":
        return default
    else:
        return input_val
