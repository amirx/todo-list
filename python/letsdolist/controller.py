import datetime

from letsdolist.db_entities import Task, SessionFactory


class Controller:
    def __init__(self, session_factory: SessionFactory):
        self._session_factory = session_factory

    def update_task(self, task: Task):
        session = self._session_factory.get_session()
        session.merge(task)
        session.commit()
        session.close()

    def add_task(self, title, description, due_date):
        session = self._session_factory.get_session()
        task = Task(title=title, description=description, due_date=due_date)
        session.add(task)
        session.commit()
        session.close()

    def get_task(self, id: int):
        session = self._session_factory.get_session()
        task = session.query(Task).get(id)
        session.close()
        return task

    def get_tasks(self):
        session = self._session_factory.get_session()
        task_query = session.query(Task).filter(Task.done_date == None)
        task = task_query.all()
        session.close()
        return task

    def mark_done(self, task_id: int, date: datetime):
        session = self._session_factory.get_session()
        task: Task = session.query(Task).get(task_id)
        task.done_date = date
        session.commit()
        session.close()

