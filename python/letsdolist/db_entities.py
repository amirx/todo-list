from sqlalchemy import Column, String, Date, Integer, Numeric

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session

Base = declarative_base()

class SessionFactory:

    def __init__(self, db_specs):
        self.engine = create_engine(db_specs)
        self._SessionFactory = sessionmaker(bind=self.engine)
        Base.metadata.create_all(self.engine)

    def get_session(self) -> Session:
        return self._SessionFactory()


class Task(Base):
    __tablename__ = 'PendingTasks'
    id = Column(Integer, primary_key=True)
    title = Column('title', String(32))
    description = Column('description', String(32))
    due_date = Column('due_date', Date)
    done_date = Column('done_date', Date)
