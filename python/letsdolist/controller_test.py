from datetime import date

from sqlalchemy import create_engine

from letsdolist.controller import Controller
from letsdolist.db_entities import Task, SessionFactory


def test_we_can_update():
    sessionFactory = SessionFactory('sqlite://')
    controller = Controller(sessionFactory)

    the_id = 1

    pt = Task(id=the_id, title="name", description="desc", due_date=date(2021, 1, 1))

    session = sessionFactory.get_session()
    session.add(pt)
    session.commit()
    session.close()

    # when
    pt.title = "new_title"
    controller.update_task(pt)

    session = sessionFactory.get_session()
    saved_pt = session.query(Task).get(the_id)
    assert saved_pt.title == "new_title"
    session.close()
