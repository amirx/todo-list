from letsdolist.views import get_int_value

class TestRegex:
    def test_get_task_value(self):
        task, job = get_int_value("123 d")
        assert task == 123
        assert job == "d"

    def test_no_number_gets_none(self):
        val = get_int_value(" d")
        assert val is None
